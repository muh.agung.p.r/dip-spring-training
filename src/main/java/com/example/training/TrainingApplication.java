package com.example.training;

import com.example.training.models.Employee;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingApplication.class, args);

//		//instance obj model
//		Employee dataEmployee = new Employee();
//		//set attribute values
//		dataEmployee.setName("agung");
//		dataEmployee.setAddress("kota kembang");
//		dataEmployee.setEmail("agung@agung.com");
//		dataEmployee.setPhone("081111111");
//		dataEmployee.setId("001");
//
//		String employeeName = dataEmployee.getName();
//		String employeeAddress = dataEmployee.getAddress();
//		String employeeEmail = dataEmployee.getEmail();
//		String employeePhone = dataEmployee.getPhone();
//
//		System.out.println("isi value: ");
//		//System.out.println(dataEmployee.getName());
//		System.out.println("Employee Name: " + employeeName);
//		System.out.println("Employee Address: " + employeeAddress);
//		System.out.println("Employee EMail: " + employeeEmail);
//		System.out.println("Employee Phone: " + employeePhone);


	}

}
