package com.example.training.api.payment;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.services.EmployeeService;
import com.example.training.services.PaymentService;
import com.example.training.viewmodels.AjaxResponseBody;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = ("/api/payment"))
public class PaymentApi {
    @Autowired
    PaymentService paymentService;

    @PostMapping(value = ("/search"))
    public List<Payment> searchPaymentApi(@RequestBody Payment dataParam) {
        return paymentService.getListPayment(dataParam);
    }

    @GetMapping(value = ("/list"))
    public List<Payment> listPaymentApi() {
        return paymentService.getListPayment(new Payment());
    }

    @PostMapping(value = ("/save"))
    public AjaxResponseBody savePayment(@RequestBody Payment dataParam) {

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = paymentService.savePayment(dataParam);

        if (save.getErrorMsg().equalsIgnoreCase("-")) {
            responseBody.setStatusCode("201");
        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;
    }

    @PostMapping(value = ("/update"))
    public AjaxResponseBody updatePayment(@RequestBody Payment dataParam) {

        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave save = paymentService.updatePayment(dataParam);

        if (save.getErrorMsg().equalsIgnoreCase("-")) {
            responseBody.setStatusCode("201");
        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(save.getErrorMsg());

        return responseBody;
    }

    @PostMapping(value = ("/delete"))
    public AjaxResponseBody deletePayment(@RequestBody Payment dataParam) {
        AjaxResponseBody responseBody = new AjaxResponseBody();

        ResponseSave delete = paymentService.deletePayment(dataParam);

        if (delete.getErrorMsg().equalsIgnoreCase("-")) {
            responseBody.setStatusCode("201");
        } else {
            responseBody.setStatusCode("500");
        }
        responseBody.setMessage(delete.getErrorMsg());
        return responseBody;
    }

}
