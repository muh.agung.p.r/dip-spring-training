package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.services.EmployeeService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping(value = "/employee")
public class EmployeeController {
    String title = "Employee" + InformationConstant.websiteTitle;

    @Autowired
    EmployeeService employeeService;

    //"/List"
    @GetMapping(value = "/List")
    public ResponseEntity<?> ListEmployee(){
        Employee employeeParam = new Employee();

        List<Employee> responseBody = employeeService.getListEmployee(employeeParam);

        System.out.println(responseBody);
        return ResponseEntity.ok().body(responseBody);
    }

    @GetMapping(value = "/rawList")
    public String ListEmployee(Model model){
        Employee employeeParam = new Employee();
        List<Employee> responseBody = employeeService.getListEmployee(employeeParam);
        model.addAttribute("RawData",responseBody);

        return "employee/list";
    }

    @GetMapping(value = "/list-datatable")
    public String ListEmployeeDatatable(Model model){
        Employee employeeParam = new Employee();
        List<Employee> responseBody = employeeService.getListEmployee(employeeParam);
        model.addAttribute("dataEmployee", JsonHelper.toJsonString(responseBody));
        model.addAttribute("username", "Agung");
        model.addAttribute("title", title);

        return "employee/list-datatable";
    }

    @GetMapping(value = "/add-employee")
    public String NewEmployee(Model model){
        String titleCard = "Add Employee";

        model.addAttribute("username","Agung");
        model.addAttribute("titleCard",titleCard);
        model.addAttribute("title",title);
        model.addAttribute("dataEmployee",new Employee());

        return "employee/form-employee";
    }

    @GetMapping(value = "/edit-employee")
    public String editEmployee(@RequestParam String idEmployee, Model model){
        String titleCard = "Edit Employee";

        //set parameter for filtering
        Employee  employeeParam = new Employee();
        employeeParam.setId(idEmployee);

        //get data from service
        Employee data = employeeService.getListEmployee(employeeParam).get(0);

        model.addAttribute("dataEmployee",data);
        model.addAttribute("titleCard",titleCard);
        model.addAttribute("title",title);
        model.addAttribute("username","Agung");

        return "employee/form-employee";
    }

    @PostMapping(value = "/save")
    public ResponseEntity<?> saveEmployee(){
        return ResponseEntity.ok("This is page save employee ");
    }

    @GetMapping(value = "/delete")
    public ResponseEntity<String> deleteEmployee(@RequestParam String idEmployee, Model model){
        String titleCard = "Delete Employee";

        //set parameter for filtering
        Employee  employeeParam = new Employee();
        employeeParam.setId(idEmployee);

        //get data from service
        ResponseSave data = employeeService.deleteEmployee(employeeParam);

        model.addAttribute("dataEmployee",data);
        model.addAttribute("titleCard",titleCard);
        model.addAttribute("title",title);
        model.addAttribute("username","Agung");

        return ResponseEntity.ok("This is page delete employee ");
    }

    @GetMapping(value = "/update")
    public ResponseEntity<?> updateEmployee(){
        return ResponseEntity.ok("This is page update employee ");
    }

    //Model itu dari thymeleaf nyah
    @GetMapping(value = ("list-role"))
    public String ListRoleEmployee(Model model){
        String titlepage = "Employee Role";
        String userName = "Administrator";

        model.addAttribute("title",titlepage);
        model.addAttribute("user",userName);

        return "employee/list-role";
    }
}
