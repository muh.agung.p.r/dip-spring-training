package com.example.training.controllers;

import com.example.training.utilities.InformationConstant;
import org.omg.CORBA.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/dashboard")
public class HomePageController {

    @GetMapping(value = (""))
    public String dashboard(Model model){

        String pageTitle = "Dashboard " + InformationConstant.websiteTitle;

        model.addAttribute("title",pageTitle);
        model.addAttribute("user", "Administrator");
        return "dashboard";
    }

    @GetMapping(value = "/detail-visitor")
    public ResponseEntity dashboardDetailVisitor(){
        return ResponseEntity.ok("This is the detail page for dashboard2an");
    }
}
