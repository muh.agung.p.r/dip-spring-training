package com.example.training.controllers;

import com.example.training.utilities.InformationConstant;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController {

    @RequestMapping(value = "/login", method = {RequestMethod.GET,RequestMethod.POST})
    public String ListRoleEmployee(Model model){
        String titlepage = "Employee Role";
        String userName = "Administrator";
        String pageTitle = "Dashboard " + InformationConstant.websiteTitle;

        model.addAttribute("title",pageTitle);
        model.addAttribute("user", userName);


        return "login";
    }
}
