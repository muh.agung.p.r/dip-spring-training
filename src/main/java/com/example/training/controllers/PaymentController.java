package com.example.training.controllers;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.services.EmployeeService;
import com.example.training.services.PaymentService;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JsonHelper;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.ByteArrayInputStream;
import java.util.List;

@Controller
@RequestMapping(value = "/payment")
public class PaymentController {
    String title = "Payment" + InformationConstant.websiteTitle;

    @Autowired
    PaymentService paymentService;

//    @GetMapping(value = "/list")
//    public String ListPayment(Model model) throws Exception{
//        //set parameter
//        Payment paymentParam = new Payment();
////        paymentParam.setId("1");
//        //call method document payment stream
////        paymentService.documentPaymentStream(paymentParam);
//
//        List<Payment> responseBody = paymentService.getListPayment(paymentParam);
//        model.addAttribute("RawData",responseBody);
//
//        return "payment/list";
//    }

    @GetMapping(value = "/list-datatable")
    public String ListPaymentDatatable(Model model){
        Payment paymentParam = new Payment();
        List<Payment> responseBody = paymentService.getListPayment(paymentParam);
        model.addAttribute("dataPayment", JsonHelper.toJsonString(responseBody));
        model.addAttribute("username", "Agung");
        model.addAttribute("title", title);

        return "payment/list-datatable";
    }

    @GetMapping(value = "/print-invoice")
    public ResponseEntity<?> printInvoice(@RequestParam String invoiceNumber) throws Exception{
        ByteArrayInputStream in = null;
        //set parameter
        Payment paymentParam = new Payment();
        System.out.println("invoice Param : " + invoiceNumber);

        paymentParam.setInvoiceNumber(invoiceNumber);
        in = paymentService.documentPaymentStream(paymentParam);

        HttpHeaders headers = new HttpHeaders();
        String headerValue = "attachment; filename=" + "Surat-Tagihan" + ".pdf";
        headers.add("Content-Disposition", headerValue);

        headers.setCacheControl("must-revalidate, post-check=0,pre-check=0");
        headers.setContentType(MediaType.parseMediaType("application/pdf"));

        return ResponseEntity.ok().headers(headers).body(new InputStreamResource(in));
    }

    @GetMapping(value = "/add-payment")
    public String NewPayment(Model model){
        String titleCard = "Add Payment";

        model.addAttribute("username","Agung");
        model.addAttribute("titleCard",titleCard);
        model.addAttribute("title",title);
        model.addAttribute("dataPayment",new Payment());

        return "payment/form-payment";
    }

    @GetMapping(value = "/edit-payment")
    public String editPayment(@RequestParam String idPayment, Model model){
        String titleCard = "Edit Payment";

        //set parameter for filtering
        Payment  paymentParam = new Payment();
        paymentParam.setId(idPayment);

        //get data from service
        Payment data = paymentService.getListPayment(paymentParam).get(0);

        model.addAttribute("dataPayment",data);
        model.addAttribute("titleCard",titleCard);
        model.addAttribute("title",title);
        model.addAttribute("username","Agung");

        return "payment/form-payment";
    }


}
