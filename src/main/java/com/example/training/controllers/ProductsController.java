package com.example.training.controllers;

import com.example.training.models.Payment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping(value = "/product")
public class ProductsController {

    @GetMapping(value = "/list")
    public String ListProduct(Model model){

        return "product/list";
    }
}
