package com.example.training.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

import java.sql.Timestamp;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class Employee {
    public String id;
    public String name;
    public String address;
    public String phone;
    public String email;
    public Timestamp createdDate;
    public String createBy;

   }
