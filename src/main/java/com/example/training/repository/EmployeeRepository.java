package com.example.training.repository;

import com.example.training.models.Employee;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import com.example.training.viewmodels.EmployeeAddress;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.*;

//penanda utk repository
@Repository
public class EmployeeRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall;

    public List<Employee> listEmployee(Employee  dataParam){
        //calling sp
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_LIST")
                .mapTo(Employee.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        //set query param
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataParam.getId())
                .addValue( "P_Name", dataParam.getName());

        //sp execution
        Map<String,Object> resultSp = simpleJdbcCall.execute(parameterSource);

        //get result value to obj
        List<Employee> employeeLists = (List<Employee>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);
        System.out.println("Isi Employee");
        System.out.println(employeeLists.get(0).getName());
        return employeeLists;

    }

    //nama method sama dgn nama sp
    public List<Employee> findAllEmployee(){
//        return null;
        //get current
        Date date = new Date();
        //instance
        Employee dataEmployee = new Employee();
//
//        dataEmployee.setId("1");
//        dataEmployee.setName("james bond");
//        dataEmployee.setAddress("jalan besar");
//        dataEmployee.setEmail("agung@agung.com");
//        dataEmployee.setPhone("081111111");
        List<Employee> returnValue = new ArrayList<>();
        for (int i = 0; i<10;  i++){
            dataEmployee.setName("User " + i);
            dataEmployee.setId("UID " + i);
            dataEmployee.setPhone("Phone " + i);
            dataEmployee.setEmail("EMail " + i);
            dataEmployee.setAddress("Address " + i);
            returnValue.add(i,dataEmployee);
        }

        return returnValue;
    }

    public List<Employee> findEmployeeById(String idEmployee){
        return null;
    }

    public Integer insertEmployeeAddress(EmployeeAddress dataForSave){
        dataForSave.dataEmployee.setId("");
        dataForSave.setArea("123");
        dataForSave.setHouseColor("Yellow");
        dataForSave.setCar(3);

        return null;
    }

    public Integer  insertNewEmployee(Employee dataForSave){
//        UUID uuid = new UUID();
//        dataForSave.setId(uuid.toString());
        dataForSave.setName("xxx");

        return null;
    }

    public ResponseSave insertEmployee(Employee dataForSave) {

        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_INSERT")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_EMAIL", dataForSave.getEmail())
                .addValue("P_PHONE", dataForSave.getPhone())
                .addValue("P_ADDRESS", dataForSave.getAddress())
                .addValue("P_CREATEBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

    public ResponseSave deleteEmployee(Employee idEmployee){
        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_DELETE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", idEmployee.getId());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");
        System.out.println(responseSave);

        return responseSave.get(0);
    }

    public Boolean deleteAllEmployee(){
        return null;
    }

    public ResponseSave updateEmployee(Employee dataForSave){
        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_EMPLOYEE_UPDATE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForSave.getId())
                .addValue("P_NAME", dataForSave.getName())
                .addValue("P_EMAIL", dataForSave.getEmail())
                .addValue("P_PHONE", dataForSave.getPhone())
                .addValue("P_ADDRESS", dataForSave.getAddress())
                .addValue("P_EDITBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

}
