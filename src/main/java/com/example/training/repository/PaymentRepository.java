package com.example.training.repository;

import com.example.training.models.Employee;
import com.example.training.models.Payment;
import com.example.training.utilities.InformationConstant;
import com.example.training.utilities.JdbcHelper;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PaymentRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    SimpleJdbcCall simpleJdbcCall;

    public List<Payment> findAllPayments(Payment dataParam){
        //calling sp
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PAYMENT_LIST")
                .mapTo(Payment.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        //set query param
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_PAYER", dataParam.getPayer())
                .addValue( "P_INVOICENUMBER", dataParam.getInvoiceNumber());

        //sp execution
        Map<String,Object> resultSp = simpleJdbcCall.execute(parameterSource);

        //get result value to obj
        List<Payment> paymentLists = (List<Payment>) resultSp.get(InformationConstant.REF_CURSOR_RECORDSET);
//        System.out.println("Isi Payment");
//        System.out.println(paymentLists.get(0).getName());
        return paymentLists;
    }

    public List<Payment> findPaymentsById(String idPayment){
        return null;
    }

    public ResponseSave updatePayment(Payment dataForSave){
        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PAYMENT_UPDATE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", dataForSave.getId())
                .addValue("P_INVOICENUMBER", dataForSave.getInvoiceNumber())
                .addValue("P_AMOUNT", dataForSave.getAmount())
                .addValue("P_USERID", dataForSave.getPayer())
                .addValue("P_STATUS", dataForSave.getStatus())
                .addValue("P_EDITBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

    public ResponseSave insertPayment(Payment dataForSave){
        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PAYMENT_INSERT")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_INVOICENUMBER", dataForSave.getInvoiceNumber())
                .addValue("P_AMOUNT", dataForSave.getAmount())
                .addValue("P_USERID", dataForSave.getPayer())
                .addValue("P_CREATEBY", dataForSave.getCreateBy());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");

        return responseSave.get(0);
    }

    public Integer deletePaymentById(String idPayment){
        return null;
    }

    public ResponseSave deletePayment(Payment idPayment){
        // Calling Stored Procedure
        JdbcHelper helper = new JdbcHelper();
        simpleJdbcCall = helper.useTemplate(this.jdbcTemplate)
                .spName("SP_PAYMENT_DELETE")
                .mapTo(ResponseSave.class)
                .outParameter(InformationConstant.REF_CURSOR_RECORDSET)
                .build();

        // Set Query Param for Stored Procedure Requirement
        SqlParameterSource parameterSource = new MapSqlParameterSource()
                .addValue("P_ID", idPayment.getId());

        Map<String, Object> resultSp = simpleJdbcCall.execute(parameterSource);

        // Get Result Value to Object
        List<ResponseSave> responseSave = (List<ResponseSave>) resultSp.get("p_recordset");
        System.out.println(responseSave);

        return responseSave.get(0);
    }


}
