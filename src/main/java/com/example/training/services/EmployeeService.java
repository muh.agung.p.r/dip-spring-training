package com.example.training.services;

import com.example.training.models.Employee;
import com.example.training.repository.EmployeeRepository;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository repository;

    public List<Employee> getListEmployee(Employee dataParam){
        return repository.listEmployee(dataParam);
    }

    public ResponseSave saveEmployee(Employee dataForSave){
        //operation example
//        String nameUpperCase = dataForSave.getName().toUpperCase();
//        dataForSave.setName(nameUpperCase);

        return repository.insertEmployee(dataForSave);
    }

    public ResponseSave updateEmployee(Employee dataForSave){
        //operation example
//        String nameUpperCase = dataForSave.getName().toUpperCase();
//        dataForSave.setName(nameUpperCase);

        return repository.updateEmployee(dataForSave);
    }

    public ResponseSave deleteEmployee(Employee idEmployee){
        return repository.deleteEmployee(idEmployee);
    }

    public List<Employee> getEmployee(String idEmployee){
        return repository.findEmployeeById(idEmployee);
    }
}
