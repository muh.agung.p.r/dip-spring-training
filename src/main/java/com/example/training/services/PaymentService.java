package com.example.training.services;

import com.aspose.words.Document;
import com.aspose.words.MailMergeCleanupOptions;
import com.aspose.words.SaveFormat;
import com.example.training.models.Payment;
import com.example.training.repository.PaymentRepository;
import com.example.training.utilities.ResourceHelper;
import com.example.training.viewmodels.ResponseSave;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.List;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository repository;

    public List<Payment> getListPayment(Payment dataParam){
        return repository.findAllPayments(dataParam);
    }

    public ResponseSave savePayment(Payment dataForSave){
         return repository.insertPayment(dataForSave);
    }

    public ResponseSave updatePayment(Payment dataForSave){
        return repository.updatePayment(dataForSave);
    }

    public ResponseSave deletePayment(Payment idPayment){
        return repository.deletePayment(idPayment);
    }

    public List<Payment> getPayment(String idPayment){
        return repository.findPaymentsById(idPayment);
    }

    public ByteArrayInputStream documentPaymentStream(Payment dataParam) throws Exception{
        //get data row from database
        Payment paymentObject = repository.findAllPayments(dataParam).get(0);

        //setup path folder for reading resource
        String  parentPath = ResourceHelper.getResourcePath();
        String pathFile = ResourceHelper.getFilePathFromResource("template-surat-tagihan",".docx");

        //config/init doc aspose lib
        Document doc = new Document(pathFile);
        System.out.println("IO Address : " + doc);

        doc.getMailMerge().setTrimWhitespaces(true);
        doc.getMailMerge().setCleanupOptions(MailMergeCleanupOptions.REMOVE_UNUSED_FIELDS | MailMergeCleanupOptions.REMOVE_CONTAINING_FIELDS
                    | MailMergeCleanupOptions.REMOVE_EMPTY_PARAGRAPHS);

        String[]  toMerge  =  {
                "PAYER","INVOICENUMBER","AMOUNT","STATUS","CREATEDATE","CREATEBY","COMPANY"
        };
        doc.getMailMerge().execute(toMerge,  new Object[]{
                paymentObject.getPayer() == null ? "-" : paymentObject.getPayer(),
                paymentObject.getInvoiceNumber() == null ? "-" : paymentObject.getInvoiceNumber(),
                paymentObject.getAmount() == null ? "-" : paymentObject.getAmount(),
                paymentObject.getStatus() == null ? "-" : paymentObject.getStatus(),
                paymentObject.getCreateDate() == null ? "-" : paymentObject.getCreateDate(),
                paymentObject.getCreateBy() == null ? "-" : paymentObject.getCreateBy(),
                "PT. DAYA INDOSA PRATAMA"
        });

        String generatedFileName = "Surat Tagihan_" + paymentObject.getInvoiceNumber();
        String finalFileName = parentPath + "/" + generatedFileName;
        String tmpWordDoc = finalFileName + ".docx";
        String wordDoc = finalFileName + ".docx" ;
        String pdfDoc = finalFileName + ".pdf" ;

        doc.save(wordDoc);
        doc.save(tmpWordDoc);
        //save sas pdf from word
        doc.save(pdfDoc, SaveFormat.PDF);
        byte[] bFile = ResourceHelper.readBytesFromFile(pdfDoc);


        return new ByteArrayInputStream(bFile);
    }
}
