package com.example.training.utilities;

public class InformationConstant {
    public static final String websiteTitle = " || PT Daya Indosa Pratama";
    public static final String DB_SCHEMA_NAME = "SYSTEM";
    public static final String REF_CURSOR_RECORDSET = "p_recordset";

public static final String TMP_RESOURCE_FILE = "foo.txt";
public static final String PRINT_TEMPLATE_PATH = "/document-print-temp/";
}
