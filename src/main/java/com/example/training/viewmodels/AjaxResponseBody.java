package com.example.training.viewmodels;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class AjaxResponseBody {
    public String statusCode;
    public String message;
}
