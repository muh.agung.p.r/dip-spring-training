//console.log("list-datatables.js");

let tblName = $("#tableEmployee");

$(document).ready(function() {
    initDatatable(dataEmployeeParse);
});

// Datatable Init
function initDatatable(data) {
    return tblName.DataTable({

        "order": [],
        "ordering": true,
        "searching": true,
        "pageLength": 5,
        "data": data,
        "lengthChange": false,
        "paging": true,
        "destroy": true,
        "scrollX": true,
        "responsive": true,
        'autoWidth': false,
        "dom": 'Bfrtip',
        "buttons": [],
        "processing": true,
        "columns": [
            {
                "data":"id",
                "width":"3%"
            },
            {
                "data":"name",
                "width":"7%"
            },
            {
                "data":"address",
                "width":"5%",
                "render": data => {
                    if (data === null){
                        return "-";
                    }
                    else{
                        return data;
                    }
                }
             },
             {
                 "data":"phone",
                "width":"5%"
             },
             {
                 "data":"createBy",
                 "width":"5%",
                 "render": data => {
                      if (data === null){
                          return "-";
                      }
                      else{
                          return data;
                      }
                   }
             },
             {
                "searchable": false,
                "orderable": false,
                "width": "4%",
                "data": null,
                render: function (data, type, row) {
                    console.log(data.id);
                    let btnLink = "";
                    btnLink = btnLink + '<a class="btn btn-info" type="button"  href="/employee/edit-employee?idEmployee=' + data.id + '">Edit</a>  ';
                    btnLink = btnLink + ' <a href="javascript:void(0)" type="button" data-id_delete="' + data.id + '" id="btn-delete" class="btn btn-danger">Delete</a>';
                    return btnLink;
                }
              },

        ]
//        "aoColumns": [
//            {
//                "width": "2%",
//                "mData": "id"
//            },
//            {
//                "width": "6%",
//                "mData": "name"
//            },
//            {
//                "width": "6%",
//                "mData": "address"
//            },
//            {
//                "width": "6%",
//                "mData": "phone"
//            },
//            {
//                "width": "6%",
//                "mData": "email"
//            },
//            {
//                "searchable": false,
//                "orderable": false,
//                "width": "4%",
//                "data": null,
//                render: function (data, type, row) {
//                    console.log(data.id);
//                    let btnLink = "";
//                    btnLink = btnLink + '<a class="btn btn-info" type="button"  href="/employee/edit-employee?idEmployee=' + data.id + '">Edit</a>  ';
//                    btnLink = btnLink + ' <a href="javascript:void(0)" type="button" data-id_delete="' + data.id + '" id="btn-delete" class="btn btn-danger">Delete</a>';
//                    return btnLink;
//                }
//            },
//        ],
    });
}